/**
 * 
 */
package org.fam.quannh;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;



/**
 * Service for music album downloader.
 * Connect to url and get html page then find direct download page
 * 
 * @author quannh (quannh.is55@gmail.com)
 * @version 1.0
 */
public class MusicAlbumsDownloadService {
	private URL albumURL; // webpage url
	private List<Element> trackElement;
	private List<String> trackUrls;
	private Document document; // Jsoup HTML document
	
	/**
	 * Non-arg constructor : Init values.
	 */
	public MusicAlbumsDownloadService () {
	}
	
	/**
	 * Connect and get document from url.
	 * @return document from url or null
	 */
	public Document connect () {
		try {
			URLConnection conn = albumURL.openConnection();
			conn.connect();
			document = Jsoup.connect(albumURL.toString()).get();
			return document;
		} catch (MalformedURLException e) {
			// TODO notify user of error
			e.printStackTrace();
		}catch (IOException e) {
			// TODO notify user of error
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Find all track download page URL
	 * 
	 * @return list of url
	 */
	public List<String> findAllTrackURL () {
		// TODO find all track download page URL from album page
		// Sellector : td>span.gen>a[target]
		String cssQuery = new String("td>span.gen>a[target]");
		
		Elements trackElements = document.select(cssQuery);
		trackElement = trackElements.subList(0, trackElements.size());
		
		trackUrls = new ArrayList<String>();
		for (Element element : trackElement) {
			trackUrls.add(element.attr("href"));
		}
		
		return trackUrls;
	}

	/**
	 * @return the url
	 */
	public URL getAlbumUrl() {
		return albumURL;
	}

	/**
	 * Validate URL by  Apache Commons Validator
	 * @param url the url to set
	 * @throws Exception 
	 */
	public boolean isUrl (URL albumURL) throws IOException {
		UrlValidator validator = new UrlValidator();
		if (validator.isValid(albumURL.toString())){
			return true;
		} else {
			throw new MalformedURLException();
		}
	}
	
	/**
	 * 
	 * @param albumURL
	 * @throws IOException
	 */
	public void setAlbumUrl(URL albumURL) throws IOException {
		if (isUrl(albumURL)){
			this.albumURL = albumURL;
		} else throw new MalformedURLException();
	}

	/**
	 * @return the document
	 */
	public Document getDocument() {
		return document;
	}

	/**
	 * @param document the document to set
	 */
	public void setDocument(Document document) {
		this.document = document;
	}
}
