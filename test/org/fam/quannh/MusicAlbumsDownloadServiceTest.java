/**
 * 
 */
package org.fam.quannh;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.fam.quannh.MusicAlbumsDownloadService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



/**
 * @author quannh (quannh.is55@gmail.com)
 *
 */
public class MusicAlbumsDownloadServiceTest {
	private static URL albumURL;
	private static Document document;
	private static MusicAlbumsDownloadService musicAlbumDownloadService;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		albumURL = new URL(new String("http://playlist.chiasenhac.com/nghe-album/wake~linkin-park~1009106.html"));
		document = Jsoup.connect(albumURL.toString()).get();
		
		musicAlbumDownloadService = new MusicAlbumsDownloadService();
		musicAlbumDownloadService.setAlbumUrl(albumURL);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	/**
	 * Test connect to get document from url
	 */
	public void testConnect() throws IOException {
		/* Because a webpage can also dynamic (ie. include different advertise link) 
		 * so I only check webpage's title.
		 */
		
		//String expected = new String("Wake - Linkin Park ~ Download Lossless, 500kbps, 320kbps");
		String expected = musicAlbumDownloadService.connect().title();
		
		//String actual = Jsoup.connect(url).get().title();
		String actual = document.title();
		
		assertEquals(expected, actual);
		//fail("Can not connect to URL"); // TODO notify tester of this error
	}
	
	@Test
	/**
	 * Test find direct track URL
	 */
	public void testFindAllTrackURL () {
		/* 
		 * TODO Uncomment this 
		 * and add list of track download page URL here
		 */
		// 13 tracks in Minutes To Midnight - Linkin Park Album
		List<String> expectedTrackList = new ArrayList<String>();
		for (int i = 0; i < 13; i++) {
			expectedTrackList.add(new String ());
		}
		//expectedTrackList.add(new String ("correct download page URLs"));
		
		/* 
		 * TODO Uncomment this and 
		 * implement medthod findAllTrackDownloadPageURL 
		 * in MusicAlbumsDownloadService class.
		 */
		musicAlbumDownloadService.connect();
		List<String> actualTrackList = musicAlbumDownloadService.findAllTrackURL();
		
		// Assert number of track list
		assertEquals(13, actualTrackList.size());
		assertEquals(expectedTrackList.size(), actualTrackList.size());
		//fail("Can not find all track download page URL"); // TODO notify tester of this error
	}
	
	@Test
	/**
	 * Test find all correct track download page url
	 */
	public void testFindAllCorrectUrl () {
		// 13 tracks in Minutes To Midnight - Linkin Park Album
		List<String> expectedTrackList = new ArrayList<String>();
		expectedTrackList.add(new String ("correct download page URLs"));
		// Tobe continues add track URLs
		
		List<String> actualTrackList = musicAlbumDownloadService.findAllTrackURL();
		// Assert correct track list
		//assertEquals(expectedTrackList, actualTrackList);
		//fail("Can not find all correct track download page URL"); // TODO notify tester of this error
	}
	
	@Test
	/**
	 * Test number of file downloaded
	 */
	public void testNumberOfFileDownloaded () {
		
	}
	
	@Test
	public void testFilesDownloadedCorrectly () {
		
	}
	
	@Test
	public void testCalculateInfo () {
		
	}
}
